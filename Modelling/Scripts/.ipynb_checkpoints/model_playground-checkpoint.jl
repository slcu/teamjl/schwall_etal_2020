###--- Introduction ---###
# This file does not generate any data directly required for the publication.
# This file is designed to enable some, with little previous knowledge of Julia, to play around with the model.
# It contains extensive descriptions, and is rather self-contained.
# It is designed to be run in Juno.


###--- Downloading Packages ---###
# Run this only once, it will fetech the required packages.
# If you run into any problems, it is likely to be in this part. I am happy to help. In principle shouldn't be that hard.
using Pkg                           # Julia's package manager, allows us to download packages.
Pkg.add("DifferentialEquations")
Pkg.add("DiffEqBiological")
Pkg.add("Plots")
Pkg.add("GR")

# Two packages are added in a bit of a special way (sincet they are semi-personal). To add these do like this:
# 1) Start the julia console. can be done within Juno by Going to Juno/Julia in the top bar, then "Open REPL". It might also be already open when you start Juno.
# 2) Start the package manager. This is done, by within the REPL, hitting the ']' button. Now instead of having a green text saying "julia" you should have a blue text saying "pkg".
# 3) type "add https://gitlab.com/slcu/teamHJ/niklas/DiffEqParameters.jl.git" (and press enter). This will add "DiffEqParameters".
# 4) type "add https://gitlab.com/slcu/teamJL/torkel/simulationsupport.jl.git" (and press enter). This will add "SimulationSupport".
# 5) In principle you could add any of the previous packages (or any other) here by typing e.g. "add DifferentialEquations".

###--- PREPARATION ---###

### This parts declares that you want to use the desired package ###
using DifferentialEquations         # The package for running differential equation siumations.
using DiffEqBiological              # The package that creates biochemical reaction network models.
using DiffEqParameters              # Doesn't really do much.
using SimulationSupport             # My (personal) package with some functions I use a lot.
using Plots                         # For making plots
gr(); default(fmt = :png);          # Sets some plots default (this one should ensure that they do not get too large).

### Declares the model ###

# Sets the parameters
v0 = 0.005; # The base activity of the operon.
v = 0.1;    # The maximum activity of the operon (or technically, the maximum acitivty is v0+v).
K = 2.8;    # The Affinity constant in the hill function.
n = 4;      # The n in the hill function.
kD = 10;    # The disscoiation rate of the σV-RsiV complex.
kB = 100;   # The binding rate of σV to RsiV.
kC = 0.1;   # A constant for the rate of cleavage of the complex in the presence of lysozyme.
deg = 0.01; # The degradation rate of all components (technically the dillution rate).
L = 0.;     # The amount of lysozyme stress present in the system. In principle L and kC always occur as L*kC so one parameter. But I typically set L=0 as on and L=1 as off, and have kC so the input and be pretty values.
η = 0.3;    # The amount of noise in the system. If this value especially is increased dt in the solver have to be reduced.

#The σV model.
σV_p = [v0, v, K, n, kD, kB, kC, deg, L, η];    #Puts the parameters into an array.
σV_network = @reaction_network rnType η begin
    v0 + hill(σV,v,K,n), ∅ → (σV+A)
    deg, (σV,A,AσV) → ∅
    (kB,kD), A + σV ↔ AσV
    L*kC, AσV → σV
end v0 v K n kD kB kC deg L;                    #Declares the model. Note that 'A' means 'RsiV' and 'AσV' means ''σV-RsiV'.
σV_model = Param(σV_network,copy(σV_p))               #Bundels up the parameter values and model into a single object.

function refresh!()
    global σV_model = Param(σV_network,copy(σV_p));
end

###--- ANALYSIS ---###

# A small note on running julia programs #
# Julia programs are compiled the first time you run them. That means that the first time you run a line, it will be run, and then compiled.
# This means that the run time the first time will be = copilation time + run time.
# Hence, the first time you run a line it will take much longer time than subsequent times.
# This is most notable for the lines that takes long time (the monte carlo simulation line).
# Sometimes, I first run these with very low n and l (n=1, l=1.) to get it compiled. Then I increased n and l and run it again (this is possible without having to re-compile).

### Stochastic simulation ###
m = 25; # The number of simulations we wish to make.
l = 4000.; #The length of the simulations.

# This allows you to change parameters during the simulation. Typically I only use it to change the stress parameter, L, but would work for others. Similar exists for changing variable values.
# Shape is (:parameter,(step_time,step_size),(step_time,step_size),(step_time,step_size),:parameter,(step_time,step_size),(step_time,step_size),(step_time,step_size))
# After each parameter there is a number of pairs (step_time,step_size). Here step_time is the time the change happens at, step_size the change in the step (additive. so if L = 1 and you have (2000.,1), L becomes 2).
# This is a base case where (lysozym) stress is added at times 500 and 2850, and removed at time 2500.
stress_input = (:L,(500.,1.),(2500.,-1.),(2850.,1.));

# Runs the simulation.
# saveat is how often we save the simulation (if not included we will save at every timestep and get really large output, slogging down the computer).
# dt is the timestep. For some parameters you might get warning that dt is to large. Then reduce and run again.
#sols =  monte(σV_model,(0.,l),m,p_steps=stress_input,adaptive=false,dt=0.002,saveat=1.)
@time sols =  monte(σV_model,(0.,l),m,p_steps=stress_input,adaptive=false,dt=0.002,saveat=1.) # Preceding a command with "@times" will give you information of how long it took to run.

#Plots the solution. has the form plot(solutions, options...). Most options should be obvious.
# vars = [1] sets which variables to plot. 1 for σV, 2 for Rsiv, and 3 for σV-RsiV. Could plot several with e.g. vars=[1,3], but might risk getting messy.
plot(sols,vars=[1],ylabel="\\sigma^{V} concentration",linewidth=1.,linealpha=0.7,xtickfont=9,ytickfont=9,xlabel="Time",framestyle = :box)

### Make a bifurcation diagram ###
bif_par = :L                                        #The parameter with respect to which we want to make the bifurcation diagram.
bif_interval = (0.,2.)                              #The interval over which we wish to varry said parameter.
bif = bifurcations(σV_model,bif_par,bif_interval)   # Gebnerates the bifurcation diagram.
plot(bif)                                           # Plots the bifrucation diagram.

### Deterministic simulations
# Similar options as for the stochastic simulations
l = 4000.;
stress_input = (:L,(500.,1.),(2500.,-1.),(2850.,1.));
sol =  detsim(σV_model,(0.,l),p_steps=stress_input,saveat=1.)
plot(sol,vars=[1],ylabel="\\sigma^{V} concentration",linewidth=4.,xtickfont=9,ytickfont=9,xlabel="Time",framestyle = :box)

### Changing parameters ###
# If you wish to change a parameter, it can be done easily without going back to the model definition.
σV_model[:v0] = 0.01
# Can also use
# σV_model[:v0] += 0.05     #To increase it by 0.05.
# σV_model[:v0] -= 0.025    #To increase it by 0.025.
# σV_model[:v0] *= 2        #To double it.
# σV_model[:v0] /= 2        #To halve it.

#Performs another simulation with a new parameter.
m = 25; l = 4000.;
@time sols =  monte(σV_model,(0.,l),m,p_steps=stress_input,adaptive=false,dt=0.002,saveat=1.) # Preceding a command with "@times" will give you information of how long it took to run.
plot(sols,vars=[1],ylabel="\\sigma^{V} concentration",linewidth=1.,linealpha=0.7,xtickfont=9,ytickfont=9,xlabel="Time",framestyle = :box)

# Please note that all simulations will now have to new parameter value.
# If you want to reset the values to what was originally declared, pelase use "refresh!()".
σV_model[:v0] = 0.01
σV_model[:v] *= 3
σV_model[:kB] =+ 50
σV_model            # Running this line, and inspecting the output, will tell you the current parameter values.
refresh!()          # This resets the parameter values.
σV_model            # You will now see the original values.
