v0 = 0.005; v = 0.1; K = 2.8; n = 4;
kD = 10; kB = 100; kC = 0.1;
deg = 0.01; L = 0.;
η = 0.3
#Normal σV model, without inducable production.
σV_p = [v0, v, K, n, kD, kB, kC, deg, L, η];
σV_network = @reaction_network rnType η begin
    v0 + hill(σV,v,K,n), ∅ → (σV+A)
    deg, (σV,A,AσV) → ∅
    (kB,kD), A + σV ↔ AσV
    L*kC, AσV → σV
end v0 v K n kD kB kC deg L;
σV_model = Param(σV_network,copy(σV_p))

#Expanded σV model which allows for inducing σV and RsiV production.
oS = 0; oA = 0;
σVinducable_p = [v0, v, K, n, kD, kB, kC, deg, L, oS, oA, η];
σVinducable_network = @reaction_network rnType η begin
    v0 + hill(σV,v,K,n), ∅ → (σV+A)
    (oS,oA), ∅ → (σV,A)
    deg, (σV,A,AσV) → ∅
    (kB,kD), A + σV ↔ AσV
    L*kC, AσV → σV
end v0 v K n kD kB kC deg L oS oA;
σVinducable_model = Param(σVinducable_network,copy(σVinducable_p))

#Model of the σV network, with an extra copy of the (σV activated) σV operon.
#sV and rV is equal to 0 or 1, designating whenever the module is present or not.
sV = 0; rV = 0;
σV2_p = [v0, v, K, n, kD, kB, kC, deg, L, sV, rV, η];
σV2_network = @reaction_network rnType η begin
    v0 + hill(σV,v,K,n), ∅ → (σV+A)
    sV*(v0 + hill(σV,v,K,n)), ∅ → σV
    rV*(v0 + hill(σV,v,K,n)), ∅ → A
    deg, (σV,A,AσV) → ∅
    (kB,kD), A + σV ↔ AσV
    L*kC, AσV → σV
end v0 v K n kD kB kC deg L sV rV;
σV2_model = Param(σV2_network,copy(σV2_p))

#Declares the refresh function.
function refresh!()
    global σV_model = Param(σV_network,copy(σV_p));
    global σVinducable_model = Param(σVinducable_network,copy(σVinducable_p));
    global σV2_model = Param(σV2_network,copy(σV2_p));
end;
