Data files in these folders roughly works like this:

Simulations are typically sampled at integer timepoints (0.0, 1.0, 2.0, ..., l). Every time a stochastic system is simulated it simulated m times. For simplicity, it is ensured that every simulation runs the full length of l.

The first line in each simulation file contains the time points (0.0, 1.0, 2.0, ..., l). The time steps are separated by a comma, ','. Next comes m lines, one for each simulation from the system in this batch. Each of these lines contains l+1 fields, separated by a comma, ','. Each field contains the state of the system of that simulation, at that time point. The model consists of 3 variables (SigV, RsiV and SigV-RsiV). Each field contains the concentration values of these three components, in that order, separated by a space, ' '. 

An example file, with 4 simulations, 5 timesteps long, could look like this:

0.0, 1.0, 2.0, 3.0, 4.0, 5.0
0.4 0.1 1.2,0.5 0.15 0.95,0.55 0.05 1.2,0.45 0.15 1.0,0.35 0.2 1.3,0.4 0.25 1.5
0.4 0.1 1.2,0.45 0.25 0.85,0.50 0.15 1.25,0.4 0.1 1.3,0.45 0.25 1.1,0.45 0.35 1.4
0.4 0.1 1.2,0.4 0.2 1.25,0.35 0.25 1.1,0.65 0.2 1.2,0.55 0.25 1.2,0.2 0.15 1.3
0.4 0.1 1.2,0.65 0.4 1.15,0.7 0.05 1.4,0.6 0.25 0.9,0.4 0.3 1.3,0.35 0.3 1.6

Where the first simulation starts with [SigV] = 0.4, [RsiV] = 0.1, [SigV-RsiV]=1.2, then as time becomes 1.0 we have [SigV] = 0.5, [RsiV] = 0.15, [SigV-RsiV]=0.95 and so on.

The user never really should need to look at these files, they should be loaded and saved automatically from the scripts. There should be no need to care about this.
